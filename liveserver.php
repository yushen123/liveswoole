<?php
$server = new swoole_websocket_server("0.0.0.0", 9502);
$server->on('open', function (swoole_websocket_server $server, $request) {
    echo "server: handshake success with fd{$request->fd}\n";
    global $map;//客户端集合
    $map[$request->fd] = $request->fd;//首次连上时存起来
});

$server->on('message', function (swoole_websocket_server $server, $frame) {
    echo "receive from {$frame->fd}:{$frame->data}\n";
    // $server->push($frame->fd, "this is server");
    global $map;//客户端集合
    // $data = $frame->data;
    $data = $frame->data;
    foreach($map as $fd){
        $server->push($fd , $data);//循环广播
    }
});

$server->on('close', function ($ser, $fd) {
    echo "client {$fd} closed\n";
});

$server->start();